﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace BlockchainAssignment
{
    class BigIntCalculations
    {
        // Math.pow for large numbers (with a decimal exponent)
        public static BigInteger bigPow(int baseNum, double exponent)
        {
            int intExp = (int)Math.Floor(exponent);
            double fracExp = exponent - intExp;
            // We will shift these temp variables to allow them to be both whole numbers, so we can BigInteger.Multiply
            BigInteger temp = BigInteger.Pow(baseNum, intExp);
            double temp2 = Math.Pow(baseNum, fracExp);
            int fractionBitsForDouble = 52;
            for (int i = 0; i < fractionBitsForDouble; i++)
            {
                temp = BigInteger.Divide(temp, 2);
                temp2 *= 2;
            }
            BigInteger ret = BigInteger.Multiply(temp, (BigInteger)temp2);
            return ret;
        }

        public static BigInteger bigDiv(BigInteger big, double divisor)
        {
            int fractionBitsForDouble = 52;
            for (int i = 0; i < fractionBitsForDouble; i++)
            {
                big = BigInteger.Multiply(big, 2);
                divisor *= 2;
            }
            BigInteger ret = BigInteger.Divide(big, (BigInteger)divisor);
            return ret;
        }
    }

    
}
    

