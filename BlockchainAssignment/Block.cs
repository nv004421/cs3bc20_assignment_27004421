﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BlockchainAssignment
{
    

    class Block
    {
        /* Block Variables */
        private DateTime timestamp; // Time of creation

        private int index; // Position of the block in the sequence of blocks
        // Difficulty starts at 4.0
        private double difficulty = 1.0; // An arbitrary number of 0's to proceed a hash value
        private long targetTime = 1000; // Target time to mine block in ms
        public static int reEvaluateInterval = 10; // Reevaluates difficulty every 10 blocks
        private static BigInteger genesisTarget = BigInteger.Divide(BigIntCalculations.bigPow(2, 256), (long)Math.Pow(16, 4)); // 4 zeros initally required
        private BigInteger target;

        public String prevHash, // A reference pointer to the previous block
            hash, // The current blocks "identity"
            merkleRoot,  // The merkle root of all transactions in the block
            minerAddress; // Public Key (Wallet Address) of the Miner

        public List<Transaction> transactionList; // List of transactions in this block
        
        // Proof-of-work
        public long nonce = 0; // Number used once for Proof-of-Work and mining
        private long? correctNonce = null; // First thread to find correct nonce sets this value
        private Object nonceLock = new object();

        // Rewards
        public double reward; // Simple fixed reward established by "Coinbase"

        public BigInteger Target { get => target; set => target = value; }

        /* Genesis block constructor */
        public Block()
        {
            timestamp = DateTime.Now;
            index = 0;
            transactionList = new List<Transaction>();
            // Since 2^256 - 1 is the max SHA256 value, we calulate the target by doing 2^(256 - difficulty) - 1
            Target = genesisTarget;
            hash = Mine();
        }


        /* New Block constructor */
        // If average time and average difficulty are passed, then difficulty is adapted
        // Average time and average difficulty are averages of the last 10 blocks, to ensure it remains relevant
        public Block(Block lastBlock, List<Transaction> transactions, String minerAddress, bool multithread = false, double oldDifficulty = -1.0, long timeOfLastTen = -1) 
        {
            if (oldDifficulty != -1.0 && timeOfLastTen != -1)
                difficulty = oldDifficulty * ((double)(reEvaluateInterval * targetTime) / (double)timeOfLastTen); // Adapt difficulty according to last X times
            else if (oldDifficulty != -1.0)
                difficulty = oldDifficulty; // Keep same difficulty

            Target = BigIntCalculations.bigDiv(genesisTarget, difficulty);
            
            timestamp = DateTime.Now;

            index = lastBlock.index + 1;
            prevHash = lastBlock.hash;

            this.minerAddress = minerAddress; // The wallet to be credited the reward for the mining effort
            reward = 1.0; // Assign a simple fixed value reward
            transactions.Add(createRewardTransaction(transactions)); // Create and append the reward transaction
            transactionList = new List<Transaction>(transactions); // Assign provided transactions to the block

            merkleRoot = MerkleRoot(transactionList); // Calculate the merkle root of the blocks transactions
            if (multithread)
                hash = multithreadMine(); // Mine using multithreading (tasks)
            else
               hash = Mine(); // Conduct PoW to create a hash which meets the given difficulty requirement
        }

        /* Hashes the entire Block object */
        public String CreateHash(long? nonce = null) // By default nonce is this.nonce
        {
            if (nonce == null)
                nonce = this.nonce;

            String hash = String.Empty;
            SHA256 hasher = SHA256Managed.Create();

            /* Concatenate all of the blocks properties including nonce as to generate a new hash on each call */
            String input = timestamp.ToString() + index + prevHash + nonce + merkleRoot;

            /* Apply the hash function to the block as represented by the string "input" */
            Byte[] hashByte = hasher.ComputeHash(Encoding.UTF8.GetBytes(input));

            /* Reformat to a string */
            foreach (byte x in hashByte)
                hash += String.Format("{0:x2}", x);
            
            return hash;
        }

        // Create a Hash which satisfies the difficulty level required for PoW
        public String Mine() {
            long nonce = 0;

            String hash = CreateHash(nonce); // Hash the block

            BigInteger hashNum = BigInteger.Pow(2, 256);
            while (hashNum > Target) { // Check we haven't reached target yet
                if (correctNonce != null) // Other thread has found correct nonce
                    return null;
                lock(nonceLock) // Lock nonce so it cannot be modified
                    nonce = ++this.nonce; // Increment the nonce so we can calc the next hash
                hash = CreateHash(nonce); // Rehash with the new nonce as to generate a different hash
                hashNum = BigInteger.Parse("0" + hash, System.Globalization.NumberStyles.HexNumber); // Prepend a zero to ensure it is not interpreted as negative
            }
            correctNonce = nonce;

            return hash; // Return the hash meeting the difficulty requirement
        }

        public String multithreadMine()
        {
            List<Task> tasks = new List<Task>();
            int numPhysicalThreads = Environment.ProcessorCount;
            for (int i = 0; i < numPhysicalThreads; i++) // Init tasks / threads
            {
                tasks.Add(new Task(() => {
                    String ret = Mine();
                    if (ret != null)
                        hash = ret;
                }));
            }

            long combinations = (long)Math.Pow(16, difficulty); // Expected number of combinations until we get the correct answer
            long interval = combinations / tasks.Count(); // Interval between nonce start points
            for(int i = 0; i < tasks.Count(); i++)
                tasks[i].Start();

            Task.WaitAll(tasks.ToArray());
            nonce = (long)correctNonce;

            return hash;
        }

        

        // Merkle Root Algorithm - Encodes transactions within a block into a single hash
        public static String MerkleRoot(List<Transaction> transactionList)
        {
            List<String> hashes = transactionList.Select(t => t.hash).ToList(); // Get a list of transaction hashes for "combining"
            
            // Handle Blocks with...
            if (hashes.Count == 0) // No transactions
            {
                return String.Empty;
            }
            if (hashes.Count == 1) // One transaction - hash with "self"
            {
                return HashCode.HashTools.combineHash(hashes[0], hashes[0]);
            }
            while (hashes.Count != 1) // Multiple transactions - Repeat until tree has been traversed
            {
                List<String> merkleLeaves = new List<String>(); // Keep track of current "level" of the tree

                for (int i=0; i<hashes.Count; i+=2) // Step over neighbouring pair combining each
                {
                    if (i == hashes.Count - 1)
                    {
                        merkleLeaves.Add(HashCode.HashTools.combineHash(hashes[i], hashes[i])); // Handle an odd number of leaves
                    }
                    else
                    {
                        merkleLeaves.Add(HashCode.HashTools.combineHash(hashes[i], hashes[i + 1])); // Hash neighbours leaves
                    }
                }
                hashes = merkleLeaves; // Update the working "layer"
            }
            return hashes[0]; // Return the root node
        }

        public double getDifficulty()
        {
            return this.difficulty;
        }

        // Create reward for incentivising the mining of block
        public Transaction createRewardTransaction(List<Transaction> transactions)
        {
            double fees = transactions.Aggregate(0.0, (acc, t) => acc + t.fee); // Sum all transaction fees
            return new Transaction("Mine Rewards", minerAddress, (reward + fees), 0, ""); // Issue reward as a transaction in the new block
        }

        /* Concatenate all properties to output to the UI */
        public override string ToString()
        {
            return "[BLOCK START]"
                + "\nIndex: " + index
                + "\tTimestamp: " + timestamp
                + "\nPrevious Hash: " + prevHash
                + "\n-- PoW --"
                + "\nDifficulty Level: " + difficulty
                + "\nNonce: " + nonce
                + "\nHash: " + hash
                + "\n-- Rewards --"
                + "\nReward: " + reward
                + "\nMiners Address: " + minerAddress
                + "\n-- " + transactionList.Count + " Transactions --"
                +"\nMerkle Root: " + merkleRoot
                + "\n" + String.Join("\n", transactionList)
                + "\n[BLOCK END]";
        }
    }
}
