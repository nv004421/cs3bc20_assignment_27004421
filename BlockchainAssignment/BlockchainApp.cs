﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows.Forms;
using System.Linq;

namespace BlockchainAssignment
{
    public partial class BlockchainApp : Form
    {
        // Global blockchain object
        private Blockchain blockchain;

        // Default App Constructor
        public BlockchainApp()
        {
            // Initialise UI Components
            InitializeComponent();
            // Create a new blockchain 
            blockchain = new Blockchain();
            // Update UI with an initalisation message
            UpdateText("New blockchain initialised!");

            // Set default value for comboBox
            miningPreferenceComboBox.SelectedIndex = 0;;
        }

        /* PRINTING */
        // Helper method to update the UI with a provided message
        private void UpdateText(String text)
        {
            output.Text = text;
        }

        // Print entire blockchain to UI
        private void ReadAll_Click(object sender, EventArgs e)
        {
            UpdateText(blockchain.ToString());
        }

        // Print Block N (based on user input)
        private void PrintBlock_Click(object sender, EventArgs e)
        {
            if (Int32.TryParse(blockNo.Text, out int index))
                UpdateText(blockchain.GetBlockAsString(index));
            else
                UpdateText("Invalid Block No.");
        }

        // Print pending transactions from the transaction pool to the UI
        private void PrintPendingTransactions_Click(object sender, EventArgs e)
        {
            UpdateText(String.Join("\n", blockchain.transactionPool));
        }

        /* WALLETS */
        // Generate a new Wallet and fill the public and private key fields of the UI
        private void GenerateWallet_Click(object sender, EventArgs e)
        {
            Wallet.Wallet myNewWallet = new Wallet.Wallet(out string privKey);

            publicKey.Text = myNewWallet.publicID;
            privateKey.Text = privKey;
        }

        // Validate the keys loaded in the UI by comparing their mathematical relationship
        private void ValidateKeys_Click(object sender, EventArgs e)
        {
            if (Wallet.Wallet.ValidatePrivateKey(privateKey.Text, publicKey.Text))
                UpdateText("Keys are valid");
            else
                UpdateText("Keys are invalid");
        }

        // Check the balance of current user
        private void CheckBalance_Click(object sender, EventArgs e)
        {
            UpdateText(blockchain.GetBalance(publicKey.Text).ToString() + " DooDoo Coin");
        }


        /* TRANSACTION MANAGEMENT */
        // Create a new pending transaction and add it to the transaction pool
        private void CreateTransaction_Click(object sender, EventArgs e)
        {
            Transaction transaction = new Transaction(publicKey.Text, receiver.Text, Double.Parse(amount.Text), Double.Parse(fee.Text), privateKey.Text);

            /* Transaction Validation */
            if (publicKey.Text == receiver.Text)
            {
                UpdateText("Cannot send coins to yourself!");
                return;
            }

            double balance = blockchain.GetBalance(publicKey.Text);
            foreach (Transaction t in blockchain.transactionPool) // We also need the balance after pending transactions
            {
                if (t.senderAddress == publicKey.Text)
                {
                    balance -= (t.amount + t.fee);
                }
            }
            double afterBalance = balance - transaction.amount - transaction.fee; // Potential balance after this transaction
            if(afterBalance < 0)
            {
                UpdateText("Cannot process transaction.\nOverall transaction cost (amount & fee): " + (transaction.amount + transaction.fee) + "\nCurrent balance (including pending): " + balance);
                return;
            }

            blockchain.transactionPool.Add(transaction);
            UpdateText(transaction.ToString());
        }

        /* BLOCK MANAGEMENT */
        // Conduct Proof-of-work in order to mine transactions from the pool and submit a new block to the Blockchain
        private void NewBlock_Click(object sender, EventArgs e)
        {
            createBlock();
        }

        private void NewBlockMultithread_Click(object sender, EventArgs e)
        {
            createBlock(true); // True means enable multithreading
        }


        /* BLOCKCHAIN VALIDATION */
        // Validate the integrity of the state of the Blockchain
        private void Validate_Click(object sender, EventArgs e)
        {
            // CASE: Genesis Block - Check only hash as no transactions are currently present
            if(blockchain.blocks.Count == 1)
            {
                if (!Blockchain.ValidateHash(blockchain.blocks[0])) // Recompute Hash to check validity
                    UpdateText("Blockchain is invalid");
                else
                    UpdateText("Blockchain is valid");
                return;
            }

            for (int i=1; i<blockchain.blocks.Count-1; i++)
            {
                if (
                    blockchain.blocks[i].prevHash != blockchain.blocks[i - 1].hash || // Check hash "chain"
                    !Blockchain.ValidateHash(blockchain.blocks[i]) ||  // Check each blocks hash
                    !Blockchain.ValidateMerkleRoot(blockchain.blocks[i]) || // Check transaction integrity using Merkle Root
                    !Blockchain.validateTransactionSignatures(blockchain.blocks[i]) // Validate all transaction signatures
                )
                {
                    UpdateText("Blockchain is invalid");
                    return;
                }
            }
            UpdateText("Blockchain is valid");
        }

       


        private long createBlock(bool shouldMultithread = false, double oldDifficulty = -1.0, long timeOfLastTen = -1)
        {
            // Retrieve pending transactions to be added to the newly generated Block
            List<Transaction> transactions = blockchain.GetPendingTransactions(publicKey.Text);
            var timer = new Stopwatch();

            //B: Run stuff you want timed
            timer.Start(); // Start stopwatch to time block creation
            // Create and append the new block - requires a reference to the previous block, a set of transactions and the miners public address (For the reward to be issued)
            Block newBlock = new Block(blockchain.GetLastBlock(), transactions, publicKey.Text, shouldMultithread, oldDifficulty, timeOfLastTen); // True on end means enable multithreading
            timer.Stop();
            blockchain.blocks.Add(newBlock);
            long ms = timer.ElapsedMilliseconds;

            UpdateText(blockchain.ToString());
            return ms; // Return time taken
        }

        private void testMultithreading_Click(object sender, EventArgs e)
        {
            UpdateText("Creating 5 blocks on single thread, then 5 blocks multithreading and comparing results...");
            String text = "";
            var singleThreadTimes = new List<long>();
            var multithreadTimes = new List<long>();
            for (int i = 0; i < 5; i++)
            {
                singleThreadTimes.Add(createBlock());
                multithreadTimes.Add(createBlock(true));
            }

            text += "Single Thread Times:\n";
            foreach (long time in singleThreadTimes)
                text += time + "ms\n";
            text += "Average Time: " + (double)(singleThreadTimes.Sum() / singleThreadTimes.Count()) + "ms\n";


            text += "\nMultithread Times:\n";
            foreach (long time in multithreadTimes)
                text += time + "ms\n";
            text += "Average Time: " + (double)(multithreadTimes.Sum() / multithreadTimes.Count()) + "ms\n";

            UpdateText(text);
        }


        public long Sum(params long[] arr)
        {
            long result = 0;
            for (int i = 0; i < arr.Length; i++)
                result += arr[i];
            return result;
        }
        public double Sum(params double[] arr)
        {
            double result = 0.0;
            for (int i = 0; i < arr.Length; i++)
                result += arr[i];
            return result;
        }

        private void testAdaptiveDifficulty_Click(object sender, EventArgs e)
        {
            UpdateText("(Using Multithreading) Creating a new block and testing the time it takes");
            String text = "Current Difficulty: 1.0\n";

            var allTimes = new List<long>();
            var allDifficulties = new List<double>();
            for (int i = 0; i <= 50; i++)
            {
                long sumTimes = 0;
                if (i % Block.reEvaluateInterval == 0 && i > 0)
                {
                    for (int j = allTimes.Count() - Block.reEvaluateInterval; j < allTimes.Count(); j++)
                        sumTimes += allTimes[j];
                    text += "Average time for last " + Block.reEvaluateInterval + ": " + sumTimes / Block.reEvaluateInterval + "ms\n";
                    allTimes.Add(createBlock(true, blockchain.GetLastBlock().getDifficulty(), sumTimes));
                    text += "New difficulty: " + blockchain.GetLastBlock().getDifficulty() + "\n";
                } else
                {
                    allTimes.Add(createBlock(true, blockchain.GetLastBlock().getDifficulty()));
                }
                allDifficulties.Add(blockchain.GetLastBlock().getDifficulty());
                //text += i + ") Time: " + allTimes[i] + "ms --- Difficulty: " + allDifficulties[i] + "\n";
                UpdateText(text);
            }
            text += "\nDone...";
            UpdateText(text);
                

            
        }

        private void miningPreferenceComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            int index = miningPreferenceComboBox.SelectedIndex;
            MiningPreference preference = (MiningPreference)index;
            blockchain.preference = preference;
        }
    }
}