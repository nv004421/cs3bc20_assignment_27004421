﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlockchainAssignment
{
    enum MiningPreference
    {
        ALTRUISTIC = 0, RANDOM = 1, GREEDY = 2, MINER_FIRST = 3
    }

    class Blockchain
    {
        // List of block objects forming the blockchain
        public List<Block> blocks;

        // Maximum number of transactions per block
        private int transactionsPerBlock = 5;

        public MiningPreference preference = MiningPreference.ALTRUISTIC;

        // List of pending transactions to be mined
        public List<Transaction> transactionPool = new List<Transaction>();

        // Default Constructor - initialises the list of blocks and generates the genesis block
        public Blockchain()
        {
            blocks = new List<Block>()
            {
                new Block() // Create and append the Genesis Block
            };
        }

        // Prints the block at the specified index to the UI
        public String GetBlockAsString(int index)
        {
            // Check if referenced block exists
            if (index >= 0 && index < blocks.Count)
                return blocks[index].ToString(); // Return block as a string
            else
                return "No such block exists";
        }

        // Retrieves the most recently appended block in the blockchain
        public Block GetLastBlock()
        {
            return blocks[blocks.Count - 1];
        }



        // Retrieve pending transactions and remove from pool
        public List<Transaction> GetPendingTransactions(String minerPublicKey)
        {
            // Determine the number of transactions to retrieve dependent on the number of pending transactions and the limit specified
            int n = Math.Min(transactionsPerBlock, transactionPool.Count);

            // "Pull" transactions from the transaction list (modifying the original list)
            List<Transaction> transactions = new List<Transaction>();
            
            if(preference == MiningPreference.ALTRUISTIC)
            {
                transactions = transactionPool.GetRange(0, n);
                transactionPool.RemoveRange(0, n);
            } else if (preference == MiningPreference.RANDOM)
            {
                Random random = new Random();
                for (int i = 0; i < n; i++)
                {
                    int index = random.Next(0, transactionPool.Count());
                    transactions.Add(transactionPool[index]);
                    transactionPool.RemoveAt(index);
                }
            } else if (preference == MiningPreference.GREEDY)
            {
                for (int i = 0; i < n; i++)
                {
                    // Get index of max value
                    double biggestFee = -1.0;
                    int biggestIndex = -1;
                    for (int j = 0; j < transactionPool.Count(); j++)
                    {
                        if (transactionPool[j].fee > biggestFee)
                        {
                            biggestFee = transactionPool[j].fee;
                            biggestIndex = i;
                        }
                    }
                    transactions.Add(transactionPool[biggestIndex]);
                    transactionPool.RemoveAt(biggestIndex);
                }
            } else if (preference == MiningPreference.MINER_FIRST)
            {
                for (int i = 0; i < n; i++)
                {
                    int minerIndex = transactionPool.FindIndex(t => t.recipientAddress == minerPublicKey || t.senderAddress == minerPublicKey);
                    if (minerIndex != -1)
                    {
                        transactions.Add(transactionPool[minerIndex]);
                        transactionPool.RemoveAt(minerIndex);
                    } else // Otherwise just remove transactions in order (Altruistic)
                    {
                        transactions.Add(transactionPool[0]);
                        transactions.RemoveAt(0);
                    }
                }
            }

            // Return the extracted transactions
            return transactions;
        }

        // Check validity of a blocks hash by recomputing the hash and comparing with the mined value
        public static bool ValidateHash(Block b)
        {
            String rehash = b.CreateHash();
            return rehash.Equals(b.hash);
        }

        // Check validity of the merkle root by recalculating the root and comparing with the mined value
        public static bool ValidateMerkleRoot(Block b)
        {
            String reMerkle = Block.MerkleRoot(b.transactionList);
            return reMerkle.Equals(b.merkleRoot);
        }

        // Validates all transaction signatures
        public static bool validateTransactionSignatures(Block b)
        {
            foreach (Transaction transaction in b.transactionList)
            {
                bool isValid = Wallet.Wallet.ValidateSignature(transaction.senderAddress, transaction.hash, transaction.signature);
                if (!isValid)
                {
                    return false;
                }
            }
            return true;
        }

        // Check the balance associated with a wallet based on the public key
        public double GetBalance(String address)
        {
            // Accumulator value
            double balance = 0;

            // Loop through all approved transactions in order to assess account balance
            foreach(Block b in blocks)
            {
                foreach(Transaction t in b.transactionList)
                {
                    if (t.recipientAddress.Equals(address))
                    {
                        balance += t.amount; // Credit funds recieved
                    }
                    if (t.senderAddress.Equals(address))
                    {
                        balance -= (t.amount + t.fee); // Debit payments placed
                    }
                }
            }
            return balance;
        }

        // Output all blocks of the blockchain as a string
        public override string ToString()
        {
            return String.Join("\n", blocks);
        }
    }
}
